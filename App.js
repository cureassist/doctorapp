
import React, { Component } from 'react';
import Route from './src/components/routes'
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    Platform,
    PermissionsAndroid
} from 'react-native';

import styles from './src/components/styles/styles'
import Nav from './src/components/navigation/tabNavigatio'
import RoutePath from './src/components/routes'



const MyStatusBar = ({ backgroundColor, ...props }) => (
    <View style={[styles.statusBar, { backgroundColor }]}>
        <StatusBar translucent backgroundColor={backgroundColor} {...props} />
    </View>
);

export default class App extends Component {


    async  GetAllPermissions() {
        try {
            if (Platform.OS === "android") {
                const userResponse = await PermissionsAndroid.requestMultiple([
                    PermissionsAndroid.PERMISSIONS.CAMERA,
                    PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
                    PermissionsAndroid.PERMISSIONS.RECORD_AUDIO,
                    PermissionsAndroid.PERMISSIONS.CALL_PHONE,
                    PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                    PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE
                ]);
                return userResponse;
            }
        } catch (err) {
            console.log(err);
        }
        return null;
    }


    componentDidMount() {
        this.GetAllPermissions();
    }

    render() {
        return (
            <View style={styles.container}>
                <MyStatusBar backgroundColor="#3BAD87" barStyle="light-content" />
                <RoutePath />
            </View>
        )



    }
}

