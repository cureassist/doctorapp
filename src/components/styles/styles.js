import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { StyleSheet, StatusBar } from 'react-native';

const STATUSBAR_HEIGHT = Platform.OS === 'ios' ? 20 : StatusBar.currentHeight;
const APPBAR_HEIGHT = Platform.OS === wp('10%') ? 44 : wp('15%');

export default StyleSheet.create({
    container: {
        flex: 1,
    },
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    statusBar: {
        height: STATUSBAR_HEIGHT,
    },
    titteStyle: {
        color: 'black',
        fontWeight: 'bold'
    },
    footerStyle: {
        paddingVertical: 20,
        borderTopWidth: 1,
        borderColor: "#CED0CE"
    },
    seperatorStyle: {
        height: 1,
        width: "84%",
        backgroundColor: "#CED0CE",
        marginLeft: "14%"
    },
    appBar: {
        backgroundColor: "#3BAD87",
        height: APPBAR_HEIGHT,
        width:wp('100%'),
        justifyContent: 'center',
        alignItems: 'center'
    },
    appBarText: {
        fontSize: wp('5%'),
        color: '#F2F2F2',
      
        
    }
})