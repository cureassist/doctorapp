import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { StyleSheet, StatusBar } from 'react-native';


export default StyleSheet.create({
  
    titteStyle: {
        color: 'black',
        fontWeight: '300',
        fontSize:hp('3%')
    },
    listStyle: {
        backgroundColor: '#FFFFFF',
        width: wp('100%'),
     
    },
    itemSeperatr: {
        height: 0.5,
        width: '100%',
        backgroundColor: '#C8C8C8'
    },
    mainContainer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    SectionHeaderStyle: {
        backgroundColor: '#FFFFFF',
        fontSize: 20,
        padding: 5,
        color: '#000',
    },
    SectionListItemStyle: {
        fontSize: 15,
        padding: 15,
        color: '#000',
        backgroundColor: '#FFFFFF',
    },
    
})