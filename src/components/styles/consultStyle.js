import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { StyleSheet, StatusBar } from 'react-native';

const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default StyleSheet.create({

    container: {
       /* marginTop: StatusBar.currentHeight,*/
        backgroundColor: "white"
    },
    scene: {
        flex: 1,
        
        backgroundColor: '#F2F2F2'
    },
    mainContainer: {
        height: '100%',
        width: '100%',
        flex: 1,
        backgroundColor: '#F2F2F2',
    },

    headingBlock: {

        backgroundColor: 'white',
        marginTop: wp('3%'),
        marginRight: wp('3%'),
        marginLeft: wp('3%'),
        borderRadius: wp('1%'),
        height: hp('20%'),
        width: wp('94%'),

    },
    SubHeadingStyle: {

        fontStyle: 'normal',
        color: 'black',
        fontSize: wp('3%'),
        fontWeight: 'normal',
        marginLeft: wp('35%'),

        opacity: .9,
        marginTop: wp('7%'),
        fontFamily: "Normal",
        letterSpacing: 1,
        lineHeight: 20,
    },
    PriceText: {
        textDecorationStyle: 'solid',
        position: 'absolute',
        marginTop: hp('14%'),
        fontSize: wp('4%'),
        marginLeft: wp('40%'),
    },
    Text: {
        textDecorationStyle: 'solid',
        position: 'absolute',
        marginTop: hp('3%'),
        fontSize: wp('4%'),
        marginLeft: wp('40%'),
    },
    years: {
        textDecorationStyle: 'solid',
        position: 'absolute',
        marginTop: hp('6%'),
        fontSize: wp('3%'),
        marginLeft: wp('40%'),
        color: '#F44336'
    },
    specialist: {
        textDecorationStyle: 'solid',
        position: 'absolute',
        marginTop: hp('9%'),
        fontSize: wp('3%'),
        marginLeft: wp('40%'),
    },

    Button: {
        borderWidth: 1,
        padding: hp('2%'),
        backgroundColor: '#04D9D9',
        height: hp('6%'),
        width: wp('25%'),
        marginLeft: wp('65%'),
        marginTop: wp('20%'),
        position: 'absolute',

    },
    labelStyle: {
        color: 'white'
    },
    imageStyle: {
        flex: 1,
        position: 'absolute',
        borderRadius: wp('1%'),
        top: wp('3.5%'),
        left: wp('3.5%'),
        height: hp('16%'),
        width: wp('30%'),
    },
    indicatorStyle: {
        backgroundColor: '#FFFFFF'
    },
    tabStyle: {
        backgroundColor: '#3BAD87',
        height: APPBAR_HEIGHT,
    },
    consultText: { textAlign: 'center', fontSize: wp('3%') }
})