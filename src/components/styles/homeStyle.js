import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { StyleSheet, StatusBar } from 'react-native';


const APPBAR_HEIGHT = Platform.OS === 'ios' ? 44 : 56;

export default StyleSheet.create({
    nameText: {
        padding: hp('2%'),
        fontStyle: 'normal',
        fontSize: hp('3%'),
        fontFamily: 'sans-serif',
    },
    text: {
        marginLeft: hp('2%'),
    },
    goOnline: {
        flex: 1,
        height: hp('10%'),

        backgroundColor: 'blue',
        marginLeft: hp('2%'),
        fontStyle: 'normal',
        fontSize: hp('2%'),
        fontFamily: 'sans-serif-light',

    },
    toggle: {

        alignItems: 'flex-end',
        marginRight: wp('2%'),
        marginTop: hp('-3%')


    },
    historyBoard: {
        flexDirection: "row",
        justifyContent: 'space-between',
        marginTop: hp('5%'),
    },
    pending: {
       
        paddingLeft: wp('2%'),
        paddingRight: wp('2%'),
        paddingTop: hp('1%'),
        backgroundColor: '#fff',
        marginLeft: wp('4%'),
        width: wp('44%'),
        height: hp('15%'),
        borderRadius: wp('4%'),
        marginBottom: hp('2%'),
        
    },
    completed: {

        backgroundColor: '#fff',
        paddingLeft: wp('2%'),
        paddingRight: wp('2%'),
        paddingTop: hp('1%'),
        marginRight: wp('4%'),
        width: wp('44%'),
        height: hp('15%'),
        borderRadius: wp('4%'),
        marginBottom: hp('2%'),
    

    },
    pendingText: {
      
        color: '#F24535',
        fontWeight: 'bold',
        fontSize: hp('2%'),
        letterSpacing: 2,
        //marginLeft: wp('18%'),
        alignSelf:'flex-end'
        //marginTop: hp('2%'),
    },
    completedText: {
       // marginTop: hp('2%'),
        color: '#03AB0E',
        fontWeight: 'bold',
        fontSize: hp('2%'),
        letterSpacing: 2, 
      //  marginLeft: wp('12%'),
        alignSelf:'flex-end'
    },
    completedCount: {
        color: '#03AB0E',
        fontSize: hp('3%'),
        alignSelf:'flex-end',
        marginBottom:hp('2%'),
    },
    pendindCount: {
        color: '#F24535',
        fontSize: hp('3%'),
        alignSelf:'flex-end',
        marginBottom:hp('2%'),
    },
    earringTitle: {

        backgroundColor: 'white',
        padding: wp('2%'),
        marginRight: wp('4%'),
        width: wp('92%'),
        height: hp('15%'),
        borderRadius: wp('4%'),
        marginBottom: hp('2%'),
        marginLeft: wp('4%'),
    },
    earningText:
        {
            marginTop: hp('2%'),
            color: '#000',
            fontWeight: 'bold',
            fontSize: hp('2%'),
            letterSpacing: 2,
            marginLeft: wp('2%'),
        },
    price: {
        marginTop: hp('2%'),
        color: '#000',
        fontFamily: 'sans-serif-light',
        fontSize: hp('3%'),
        letterSpacing: 2,
        marginLeft: wp('2%'),
    },
    docImage: {
        width: wp('7%'),
        height: hp('4%'),
        alignItems: 'flex-start'
    }
})