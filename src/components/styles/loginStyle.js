import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { StyleSheet } from 'react-native';


export default StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'red',

    },
    contentContainer: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: 'rgba(0,0,0,0.5)',

    },
    inputContainer: {
        borderColor: 'black',
        backgroundColor: '#FFFFFF',
        borderRadius: wp('10%'),
        borderWidth: 2,
        height: hp('6%'),
        marginBottom: hp('3%'),
        flexDirection: 'row',
        alignItems: 'center',
        width: '80%',
        padding: wp('3%'),
    },
    inputs: {
        height: hp('6%'),
        marginLeft: wp('3%'),

        flex: 1,
    },
    inputIcon: {
        width: wp('6%'),
        height: hp('4%'),
        marginLeft: wp('3%'),
        justifyContent: 'center'
    },
    buttonContainer: {
        marginTop: hp('2%'),
        height: hp('6%'),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: hp('3%'),
        width: wp('50%'),
        borderRadius: 30,
    },
    setPassbuttonContainer: {
        marginTop: hp('8%'),
        height: hp('6%'),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: hp('3%'),
        width: wp('50%'),
        borderRadius: 30,
    },
    logbuttonContainer: {
        height: hp('6%'),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: hp('3%'),
        width: wp('50%'),
        borderRadius: 30,
    },
    regButtonContainer: {
        marginTop: hp('12%'),
        height: hp('6%'),
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: hp('3%'),
        width: wp('50%'),
        borderRadius: 30,
    },
    loginButton: {
        backgroundColor: "#5779C8",
    },
    TextColor: {
        color: 'white',
    },
    backgoundWrapper:
        {
            width: '100%',
            height: '100%',
            backgroundColor: 'rgba(0,0,0,0.9)',
        },
    logoWrapper: {
        //Styles
        marginBottom: hp('5%'),
        marginTop: hp('8%'),
    },
    separator: {
        marginVertical: hp('1%'),
        borderBottomColor: '#FFFFFF',
        borderBottomWidth: hp('0.5%'),
    },
    ForgotLinkContainer: {

        height: hp('6%'),
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginLeft: wp('20%'),


    },

    resetLinkContainer: {
        height: hp('6%'),
    },
    linkView: {
        marginTop: hp('1%'),
        flexDirection: "row",
        justifyContent: 'space-between'
    },
    customizedSocialIcon: {
        width: wp('10.5%'),
        height: hp('5.7%'),
        borderRadius: hp('5%'),
        overflow: "hidden",
        borderWidth: 3,
        marginTop: hp('1%'),
        marginLeft: wp('1%'),
    },
    customizedSocialIconText: {
        color: 'white',
        marginTop: hp('1%'),
        marginLeft: wp('1%'),
    },
    iconPos:
        {
            flexDirection: 'column',
            marginRight: wp('4%')
        },
    newMemberText: {
        color: 'white',
        marginBottom: hp('1%'),
        marginLeft: hp('7%'),

    },
    regMemberText: {
        color: 'white',
        marginLeft: hp('7%'),

    },
    socialIconWrapper: {
        flex: 1,
        flexDirection: 'row',
        marginTop: hp('1%'),
    },
    RegButton: {
        backgroundColor: "#E75047",
    },
    innerContainer: {
        margin: 8,
        marginTop: Platform.select({ ios: 8, android: 32 }),
        flex: 1,
    }
})