import React from 'react';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { StyleSheet, Text, View, Button, Image, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator, createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';
import ConsultPage from '../screens/consult'
import Settings from '../screens/setting'
import HomePage from '../screens/home'
import Login from '../screens/login'

import { Actions } from 'react-native-router-flux'


class HomeScreen extends React.Component {
    render() {
        return (
       
            <Login />
         
 
        );
    }
}
class Consult extends React.Component {
    render() {
        return (
            <ConsultPage/>
        );
    }
}

class SettingScreen extends React.Component {
    render() {
        return (
            <View style={styles.container}>
                <Settings/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
});
const TabNavigator = createMaterialBottomTabNavigator(
    {
        Home: {
            screen: HomeScreen,
            navigationOptions: {
                tabBarLabel: 'Home',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{ color: tintColor }]} size={25} name={'ios-home'} />
                    </View>),
            }
        },
        Consults: {
            screen: Consult,
            navigationOptions: {
                tabBarLabel: 'Consultation',
                labeled: true,
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{ color: tintColor }]} size={25} name={'ios-clipboard'} />
                    </View>),

            }
        },
        
        Setting: {
            screen: SettingScreen,
            navigationOptions: {
                tabBarLabel: 'Settings',
                tabBarIcon: ({ tintColor }) => (
                    <View>
                        <Icon style={[{ color: tintColor }]} size={25} name={'ios-settings'} />
                    </View>),
            }
        },
       
    },
    {
        shifting: false,
        initialRouteName: "Home",
        activeColor: '#f0edf6',
        inactiveColor: '#226557',
        barStyle: { backgroundColor: '#3BAD87' },

    },
);

export default createAppContainer(TabNavigator);  