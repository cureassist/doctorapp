import React from 'react'
import { BackHandler, Alert } from 'react-native'
import { Actions,Router, Scene,ActionConst } from 'react-native-router-flux'
import Consult from '../components/screens/consult';
import Home from '../components/screens/home'
import VideoHome from '../videoComponent/Home'
import videoCallPage from '../videoComponent/Video'
import Setting from '../components/screens/setting'
import Main from '../components/screens/main'
import VideoCall from '../components/screens/videoCall'
import Test from '../components/screens/test'

const scenes = Actions.create(
   
        <Scene key="root">
            <Scene key="home" component={Main} title="DocApp" initial={true} hideNavBar={true} />
            <Scene key="consultation" component={Consult} hideNavBar={true}  />
            <Scene key="setting" component={Setting} hideNavBar={true} />
            <Scene key="videoCall" component={VideoCall} hideNavBar={true} />
            <Scene key="videoHome" component={VideoHome} title="CureAssist Video Call"   />
            <Scene key="test" component={Test} title="Video Feed" />
			<Scene key="video" component={videoCallPage} title="Video Feed" type={ActionConst.RESET} hideNavBar={true} />
        </Scene>

)
export default class RoutePath extends React.Component {
  
  
  
    render () {
      return <Router scenes={scenes} />
    }
  }