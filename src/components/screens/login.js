import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    TextInput,
    Button,
    TouchableHighlight,
    Image,
    Alert,
    ImageBackground,
    TouchableOpacity,
    ToastAndroid
} from 'react-native';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp
} from 'react-native-responsive-screen';
import styles from '../styles/loginStyle';
import { Actions } from 'react-native-router-flux'
import { MaterialCommunityIcons as Icon } from 'react-native-vector-icons';
import { TextField } from 'react-native-material-textfield';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import passwordValidator from 'password-validator'

export default class LoginPage extends Component {

    constructor(props) {
        super(props);
        this.onFocus = this.onFocus.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
        this.onChangeText = this.onChangeText.bind(this);

        this.onSubmitUserId = this.onSubmitUserId.bind(this);
        this.onSubmitPassword = this.onSubmitPassword.bind(this);
        this.onAccessoryPress = this.onAccessoryPress.bind(this);

        this.onErrorUserID = this.onErrorUserID.bind(this);
        this.onErrorPassword = this.onErrorPassword.bind(this);
          
        this.userRef = this.updateRef.bind(this, 'userId');
        this.passwordRef = this.updateRef.bind(this, 'password');

        this.renderPasswordAccessory = this.renderPasswordAccessory.bind(this);

        this.state = {
            secureTextEntry: true,
          
        };

    }



    onFocus() {
        let { errors = {} } = this.state;

        for (let name in errors) {
            let ref = this[name];

            if (ref && ref.isFocused()) {
                delete errors[name];
            }
        }

        this.setState({ errors });
    }

    onChangeText(text) {
        ['userId', 'password']
            .map((name) => ({ name, ref: this[name] }))
            .forEach(({ name, ref }) => {
                if (ref.isFocused()) {
                    this.setState({ [name]: text });
                }
            });
    }

    onAccessoryPress() {
        this.setState(({ secureTextEntry }) => ({ secureTextEntry: !secureTextEntry }));
    }

    homePage() {
        Actions.setting()
    }

    onSubmitUserId() {
        this.password.focus();
    }

    onSubmitPassword() {
        this.password.blur();
    }
    onErrorUserID() {
        this.userId.clear();
    }

    onErrorPassword() {
        this.password.clear();
    }

    fetchLoginAPI(emailId, passwrd) {
        console.log(emailId)
        console.log(passwrd)
        fetch('http://192.168.0.164:8000/doctors/login', {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            body: JSON.stringify({
                loginID: emailId,
                password: passwrd
            }),
        })
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson);
                if (responseJson != 'User does not exist') {
                    this.homePage();
                }
                else {
                    
                    this.onErrorEmail();
                    this.onErrorPassword();
                    ToastAndroid.showWithGravity(
                        'Invalid Email-Id Or Password',
                        ToastAndroid.LONG,
                        ToastAndroid.CENTER,
                        
                    );
                }
    
            })
            .catch((error) => {
                console.error(error);

            });
    }
    onSubmit() {
        // Create a schema
        let schema = new passwordValidator();
        // Add properties to it
        schema
            .is().min(6)                                    // Minimum length 8
            .is().max(100)                                  // Maximum length 100
            .has().uppercase()                              // Must have uppercase letters
            .has().lowercase()                              // Must have lowercase letters
            .has().digits()                                 // Must have digits
            .has().not().spaces()                           // Should not have spaces
        let errors = {};

        let userid = '';
        let validPasswrd = '';
        let passVal = '';
        ['userId', 'password']
          
           .forEach((name) => {
               let value = this[name].value();
          
               if (!value) {
                   errors[name] = 'Should not be empty';
               } 
               else {
                   if ('userId' === name) {
                       userid = this[name].value();
                   }
                   if ('password' === name) {
                        passVal = this[name].value();
                       let passValidate = schema.validate(passVal)
                       if (passValidate) {
                           validPasswrd = passVal;
                           console.log("password valid")
                          this.fetchLoginAPI(userid,validPasswrd)
                       }
                       else {
                           errors[name] = 'Password must contain minimun 6 characters atleast 1 uppercase,1 lowecase ,1 digits and no space';
                           console.log(" password Not valid")
                       }
                   }
                   if (userid != '' && validPasswrd != '') {

                   }
                   else {
                       this.onErrorUserID();
                       this.onErrorPassword();
                   }
                  
                   
               }
        });
        this.setState({ errors });
    }

    updateRef(name, ref) {
        this[name] = ref;
    }

    renderPasswordAccessory() {
        let { secureTextEntry } = this.state;

        let name = secureTextEntry ?
            'visibility' :
            'visibility-off';

        return (
            <MaterialIcon
                size={24}
                name={name}
                color='#B7CDC1'
                onPress={this.onAccessoryPress}
                suppressHighlighting={true}
            />
        );
    }


    
    render() {
        let { errors = {}, secureTextEntry } = this.state;
        return (
           
            <View style={styles.container}>

                <ImageBackground source={require('../assets/img.jpg')} style={styles.backgoundWrapper}>
                    <View style={styles.contentContainer}>
                        <View >
                            <Image source={require("../assets/cureAssit_logo.png")} style={styles.logoWrapper} />
                        </View>
                        <View style={{width:wp('80%'),}}>
                            
                            <TextField
                                ref={this.userRef}
                                autoCapitalize='none'
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                onFocus={this.onFocus}
                                onChangeText={this.onChangeText}
                                onSubmitEditing={this.onSubmitUserId}
                                returnKeyType='next'
                                label='Email-ID'
                                error={errors.userId}
                                baseColor='rgba(255, 255, 255, .90)'
                                lineWidth={2}
                                textColor='rgba(255,255,255,1)'
                                fontSize={18}
                        />

                            <TextField
                                ref={this.passwordRef}
                                secureTextEntry={secureTextEntry}
                                autoCapitalize='none'
                                autoCorrect={false}
                                enablesReturnKeyAutomatically={true}
                                clearTextOnFocus={true}
                                onFocus={this.onFocus}
                                onChangeText={this.onChangeText}
                                onSubmitEditing={this.onSubmitPassword}
                                returnKeyType='done'
                                label='Password'
                                error={errors.password}
                                
                                maxLength={30}
                                //characterRestriction={20}
                                renderRightAccessory={this.renderPasswordAccessory}
                                baseColor='rgba(255, 255, 255, .90)'
                                lineWidth={2}
                                textColor='rgba(255,255,255,1)'
                                fontSize={18}
                            />
                            
                        </View>
                        
                        <TouchableHighlight style={[styles.buttonContainer, styles.loginButton,{marginTop:hp('10%')}]}
                            onPress={this.onSubmit}
                            
                           
                            titleColor='white'>
                            <Text style={styles.TextColor}>Login</Text>
                        </TouchableHighlight>

                    </View>
                </ImageBackground>

            </View>

        )
    }
}

