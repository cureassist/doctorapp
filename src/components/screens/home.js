import React from 'react'
import { View, Text, Switch, Alert, Image, TouchableOpacity } from 'react-native';
import style from '../../components/styles/styles';
import styles from '../../components/styles/homeStyle';
import MyWebComponent from '../../components/screens/videoCall'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { Actions } from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/AntDesign'
import IonIcon from 'react-native-vector-icons/Ionicons'
import FontIcon from 'react-native-vector-icons/FontAwesome5'
export default class HomePage extends React.Component {
    constructor() {
        super();
        this.state = {
            switchValue: false,
        }
    }
    callWithoutArgument = () => {
        // Alert.alert('hi');



    };
    toggleSwitch = (value) => {
        console.log(value)

        this.setState({ switchValue: value })
        if (value) {
            Actions.videoHome();
        }
        //else {
        //    Alert.alert('offine');
        //}

    }

    render() {
        return (
            <View style={style.container}>
                <View style={style.appBar}>
                    <Text style={style.appBarText}>
                        CureAssist Doctor
                    </Text >
                </View>

                <Text style={styles.nameText} >
                    Welcome, Dr. Shanti Raju-kankipati
                </Text>


                <Text style={styles.text} > Go Online for Consultation </Text>

                <Switch
                    style={styles.toggle}
                    onValueChange={this.toggleSwitch}
                    value={this.state.switchValue}
                //onChange={this.callWithoutArgument}

                />


                <View style={styles.historyBoard}>
                    <View style={styles.pending}>
                        <FontIcon
                            raised
                            size={25}
                            name='exclamation'
                            type='font-awesome'
                            color='#f50'
                           // onPress={() => console.log('hello')}
                             />

                        <Text style={styles.pendindCount}> 0 </Text>
                        <Text style={styles.pendingText}> PENDING </Text>

                    </View>


                    <View style={styles.completed}>
                    <FontIcon
                            raised
                            size={25}
                            name='check'
                            type='font-awesome'
                            color='#03AB0E'
                            
                            onPress={() => console.log('hello')} />
                        <Text style={styles.completedCount}> 0 </Text>
                        <Text style={styles.completedText}> COMPLETED </Text>
                    </View>



                </View>



                <View style={styles.earringTitle}>
                    <Text style={styles.earningText} >
                        MY EARNINGS
                </Text>
                    <Text style={styles.price} >
                        {'\u20AC'} 1000
                </Text>

                </View>

            </View>
        )
    }
}
