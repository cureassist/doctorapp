import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { WebView } from 'react-native-webview';

// ...
export default class MyWebComponent extends Component {
    //defaultArgumentFunction = () => {
        
  //  }
    render() {
        return (
            <View style={{ flex: 1 }}>
                <WebView
                    source={{ uri: 'https://simplewebrtc.preksh.com/#CureAssit' }}
                    onLoadProgress={e => console.log(e.nativeEvent.progress)}
                    allowsBackForwardNavigationGestures={true}
                    allowsInlineMediaPlayback={true}
                    originWhitelist={['*']}
                    mixedContentMode={"compatibility"}
                    domStorageEnabled={true}
                    thirdPartyCookiesEnabled={true}
                    allowFileAccess={true}
                    allowsLinkPreview={true}
                    javaScriptEnabled={true}
                    allowUniversalAccessFromFileURLs={true}
                    startInLoadingState={true}
                    onError={syntheticEvent => {
                        const { nativeEvent } = syntheticEvent;
                        console.warn('WebView error: ', nativeEvent);
                    }} />
            </View>
        );
    }

}
