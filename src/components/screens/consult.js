import * as React from 'react';
import { View, Dimensions, StatusBar, ScrollView, TouchableOpacity, Image, Text, Alert, FlatList, ActivityIndicator} from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Avatar } from 'react-native-elements';
import styles from '../../components/styles/consultStyle';
import style from '../../components/styles/styles';
import { ListItem } from 'react-native-elements'
class PendingRoute extends React.Component {
    constructor() {
        super()
        this.state = {
            dataSource: [
                {
                    "patient_name": "Adrian",
                    "symptoms":"STIs"  
                },
                {
                    "patient_name": "Alex",
                    "symptoms": "fecal incontinence"
                },
                {
                    "patient_name": "Ellie",
                    "symptoms": "polycystic ovary syndrome"
                }
            ],
            loading: false,
            refreshing: false,
        }
    }
    renderSeparator = () => {
        return (
            <View
                style={style.seperatorStyle}
            />
        );
    };
    keyExtractor = (item, index) => index.toString()

    renderItem = ({ item }) => (
        <ListItem
            titleStyle={style.titteStyle}
            title={item.patient_name}
            subtitle={item.symptoms}
            leftAvatar=
            //{< Avatar source={{ uri: item.image }} rounded size="medium"/>}

            {< Avatar source={require("../assets/DoctorAvatar.png")} rounded size="medium" />}
            containerStyle={{ borderBottomWidth: 0}}
        
        />
    )
    handleRefresh = () => {
        this.setState(
            {
                
                refreshing: true
            },
            () => {
                this.makeRemoteRequest();
            }
        );
    };
    componentDidMount() {
     //   this.makeRemoteRequest();
    }
    makeRemoteRequest = () => {
        const url = 'http://www.json-generator.com/api/json/get/bPfMNantIO?indent=2'
        this.setState({ loading: true });
        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                //console.log(responseJson)
                this.setState({
                    dataSource: responseJson.consultation_list,
                    loading: false,
                    refreshing: false
                })

            })
            .catch((error) => {
                console.log(error)
                this.setState({ loading: false });
            })
    }

    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={style.footerStyle}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };
    render() {
        return (

            <View style={styles.scene}>

                <FlatList 

                    data={this.state.dataSource}
                    renderItem={this.renderItem}
                    /*data={[
                        { key: 'Android' },
                        { key: 'Php' }
                    ]} 
                    renderItem={({ item }) => <Text> {item.key}</Text>}*/
                    keyExtractor={this.keyExtractor}
                    ListFooterComponent={this.renderFooter}
                    onRefresh={this.handleRefresh}
                    refreshing={this.state.refreshing}
                    onEndReachedThreshold={50}
                    ItemSeparatorComponent={this.renderSeparator}
                />

            </View>
        );
    }
}
class CompletedRoute extends React.Component {
    render() {
        return (
            <View style={styles.scene} >


                <Text >Services Screen</Text>
            </View>
        );
    }
}


export default class Consultation extends React.Component {
    state = {
        index: 0,
        routes: [
            { key: 'first', title: 'Pendings' },
            { key: 'second', title: 'Completed' },
        ],
    };


    render() {
        return (
            <View style={styles.mainContainer}>

                <TabView
                    swipeEnabled={true}
                    navigationState={this.state}
                    renderScene={SceneMap({
                        first: PendingRoute,
                        second: CompletedRoute,
                    })}
                    renderTabBar={props =>
                        <TabBar
                            {...props}
                            indicatorStyle={styles.indicatorStyle}
                            style={styles.tabStyle}
                            labelStyle={styles.labelStyle}
                        />
                    }

                    onIndexChange={index => this.setState({ index })}
                    initialLayout={{ width: Dimensions.get('window').width }}
                    style={styles.container}
                />



            </View>
        );
    }
}

