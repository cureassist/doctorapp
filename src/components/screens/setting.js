import * as React from 'react';
import { View, Dimensions, StatusBar, StyleSheet, ScrollView, TouchableOpacity, Image, Text, SectionList, Alert, FlatList, ActivityIndicator } from 'react-native';

import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import { Avatar } from 'react-native-elements';
import style from '../../components/styles/styles';
import Settingstyle from '../../components/styles/settingStyle';
import { ListItem } from 'react-native-elements'
import settingStyle from '../../components/styles/settingStyle';
export default class Setting extends React.Component {
    constructor() {
        super()
        this.state = {
            dataSource: [
                {
                    "doctor_name": "Dr. Shanti Raju-kankipati",
                    "qualification": "Consultant Gynaecologist"
                }
            ]
        }
    }
    GetSectionListItem = item => {
        //Function for click on an item
        Alert.alert(item);
    };
    FlatListItemSeparator = () => {
        return (
            //Item Separator
            <View style={settingStyle.itemSeperatr} />
        );
    };
    renderItem = ({ item }) => (
        <ListItem
            titleStyle={Settingstyle.titteStyle}
            title={item.doctor_name}
            subtitle={item.qualification}
            leftAvatar=
            //{< Avatar source={{ uri: item.picture }} rounded size={100} />}
            {< Avatar source={require("../assets/woman-doctor-icon.png")} rounded size={100} />}
            containerStyle={{ borderBottomWidth: 0 }}

        />
    )

    keyExtractor = (item, index) => index.toString()
    componentDidMount() {
        //this.makeRemoteRequest();
    }
    makeRemoteRequest = () => {
        const url = 'http://www.json-generator.com/api/json/get/cfeqZlWqtK?indent=2'

        fetch(url)
            .then((response) => response.json())
            .then((responseJson) => {
                console.log(responseJson)
                this.setState({
                    dataSource: responseJson.Doctor_details,

                })

            })
            .catch((error) => {
                console.log(error)
            })
    }



    render() {
        var A = [{ 'id': '1', 'value': 'Profile' }, { 'id': '2', 'value': 'Payments' }];
        var B = [{ 'id': '4', 'value': 'Help' }, { 'id': '5', 'value': 'FAQ' },
        { 'id': '6', 'value': 'Terms & Conditions' }];
       
        return (

            <View style={settingStyle.mainContainer}>

                <View style={style.appBar}>
                    <Text style={style.appBarText}>
                        Settings
                    </Text >
                </View>
                <View style={settingStyle.listStyle}>
                    <FlatList

                        data={this.state.dataSource}
                        renderItem={this.renderItem}
                        keyExtractor={this.keyExtractor}

                    />
                </View>
                <View style={{ backgroundColor: '#FFFFFF', width: wp('100%') }}>

                    <SectionList
                        ItemSeparatorComponent={this.FlatListItemSeparator}
                        sections={[
                            { title: 'ACCOUNT SETTINGS', data: A },
                            { title: 'SUPPORT ', data: B },
         
                        ]}
                        renderSectionHeader={({ section }) => (
                            <Text style={settingStyle.SectionHeaderStyle}> {section.title} </Text>
                        )}
                        renderItem={({ item }) => (
                            <ListItem
                                title={item.value}
                                chevron={{ color: '#000' }}
                                containerStyle={{ borderBottomWidth: 0 }}
                                onPress={
                                    this.GetSectionListItem.bind(this,
                                       'Id: ' + item.id + ' Name: ' + item.value)
                                }
                            />
                       
                          
                        )}
      
                        keyExtractor={(item, index) => index}
                    />
                </View>

            </View>
        );
    }
}


